<?php

namespace App\Http\Controllers;

use App\post;
use Illuminate\Http\Request;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $logedid = Auth::id();
      $posts = post::all()->where('user_id',$logedid);
      return view('adminPanel/home', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminPanel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new post;

        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->body = $request->body;
        $post->save();

        return redirect()->route('posts.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(post $post)
    {

      $thePost = post::find($post->id);
        return view('blog.viewPost',compact('thePost'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(post $post)
    {
      return view('adminPanel.edit',compact('post'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, post $post)
    {
        $thePost = Post::find($post->id);
        $page ="";

        if (isset($request->commentCount))
        {
          $commentCount = $request->commentCount;
          $thePost->comment_count = $commentCount;
          $page = "posts.show";
        }

        if (isset($request->visitCount))
        {
          $visitCount = $request->visitCount;
          $thePost->visit_count = $visitCount;
          $page = "posts.show";
        }

        if (isset($request->title))
        {
          $thePost->title = $request->title;
          $page = "posts.index";
        }

        if (isset($request->body))
        {
          $thePost->body = $request->body ;
        }

        $thePost->save();



        return redirect()->route($page,compact('thePost'));
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(post $post)
    {
        $post->delete();

        return redirect()->back();
    }

    public function publicHomePage(Request $request)
    {
      if($request->input('type') == 'recentPosts')
      {
        $post = Post::orderBy('created_at','asc')->paginate(10);
        $orginazation = 'Top 10 most Recent Posts';
      }
      else if ($request->input('type') == 'commentPosts')
      {
        $post = Post::orderBy('comment_count','desc')->paginate(10);
        $orginazation = 'Top 10 Most Commented posts';
      }
      else if($request->input('type') == 'visitPosts')
      {
        $post = Post::orderBy('visit_count','desc')->paginate(10);
        $orginazation = 'Top 10 Most visited posts';
      }
      else
      {
        $post = Post::orderBy('created_at','asc')->paginate(10);
        $orginazation = 'Top 10 most Recent Posts';
      }

      $data = array(
        'post' => $post,
        'orginazation' => $orginazation
      );

        return view('blog/home', $data);
    }
}
