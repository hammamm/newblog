<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>@yield('title')</title>
  </head>
  <body>
    <div class="container">
      <a href="/">Go Back To Home</a>

        <div class="loginBox nav navbar-nav pull-right">
          @guest
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>
          @else
              <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>

                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </div>
              </li>
          @endguest
        </div>

        <div>
          <h1>welcom to the blog</h1>
        </div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="" class="dropdown-toggle" data-toggle="dropdown">sort posts by <span class="caret"></span> </a>
        <ul class="dropdown-menu" >
          <li><a href="{{route('getPublic',['type' => 'recentPosts'])}}">Top 10 most Recent Posts</a></li>
          <li><a href="{{route('getPublic',['type' => 'commentPosts'])}}">Top 10 Most Commented posts</a></li>
          <li><a href="{{route('getPublic',['type' => 'visitPosts'])}}">Top 10 Most visited posts</a></li>
        </ul>

      </li>

  </ul>
@if(Auth::check())
<ul class="nav navbar-nav navbar-right">
  <li><a href="{{route('posts.index')}}">Manage Blog Posts</a> </li>
</ul>
@endif
  </div>
</nav>

<div>
  @yield('content')

  <div class="footer text-center" style="margin: 20px 0 60px 0;">
    <p>&copy; Begin programming</p>
  </div>
    </div>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous">
  </script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>
