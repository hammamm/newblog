@extends('layouts.PublicHomePageTemplete')

@section('title' ,'Blog home page')

@section('content')
<div>
<h2>{{$orginazation}}</h2>
@foreach($post as $p)

<div class="well well-lg">

<h3>{{$p->title}}</h3>

<p>{{$p->body}}</p>

<br>
<br>
<p>Comment Count: {{$p->comment_count}}</p>
<p>Post created At: {{date('F d, Y', strtotime($p->created_at))}} at {{date('g:ia', strtotime($p->created_at))}}</p>
<p>visit Count: {{$p->visit_count}}</p>
<a href="{{route('posts.show',['id'=>$p->id])}}" class="btn btn-default pull-right">View Post</a>

&nbsp;
</div>



@endforeach
<div class="row text-center">
{{$post->links()}}
</div>
</div>
@endsection('content')
