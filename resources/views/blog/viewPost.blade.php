@extends('layouts.viewPostTemplete')
@section('title','View Post #'. $thePost->id)

@section('content')
<div id="fbCommentCount" style="Display:none;">
  <span class="fb-comments-count" data-href="http://newblog.test/posts/{{$thePost->id}}"></span>
</div>
<form style="Display:none;" action="{{route('posts.update',['id' => $thePost->id])}}" method="post">
  {{csrf_field()}}
  <input type="hidden" name="_method" value="PUT">

  <input type="text" name="commentCount" id="fbCommentCounts">

  <input type="text"  value="{{$thePost->visit_count}}" id="postVisitCount" name="visitCount">
</form>


<div class="row">
<h1>{{$thePost->title}}</h1>
<p>{{$thePost->body}}</p>
</div>

<div class="row text-center" id="facebookCommentContainer">
<div class="fb-comments" data-href="http://newblog.test/posts/{{$thePost->id}}" data-width="800" data-numposts="5"></div>
</div>

<script>
 let fbCommentCount = document.getElementById('fbCommentCount').getElementsByClassName('fb_comments_count');

setTimeout(function(){
  document.getElementById('fbCommentCounts').value = fbCommentCount[0].innerHTML;

let visitCount = document.getElementById('postVisitCount').value;
let visitCountPlusOne = parseInt(visitCount) + 1;
document.getElementById('postVisitCount').value = visitCountPlusOne;
  let $formVar = $('form');
  $.ajax({
    url: $formVar.prop('{{ route('posts.update' , ['id'=> $thePost->id])}}'),
    method: 'PUT',
    data:$formVar.serialize()
  });
}, 1000);


</script>
@endsection('content')
