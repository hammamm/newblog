@extends('layouts.templete')

@section('title' , 'Add New Post')

@section('content')
  <h1>Add New Post</h1>
  <div class="col-sm-8 col-sm-offset-2">

    <form class="" action="{{route('posts.store')}}" method="post">
      {{csrf_field()}}
      <div class="form-group">
        <label for="title">Title:</label>
        <input type="text" name="title" value="" class="form-control">
      </div>

      <div class="form-group">
        <label for="body">Body:</label>
        <textarea name="body" rows="8" cols="80" class="form-control"></textarea>
      </div>

      <button type="submit" name="button" class="btn btn-primary">Submit</button>
      <a href="{{route('posts.index')}}" class="btn btn-primary">Go Back</a>
    </form>

  </div>
@endsection
